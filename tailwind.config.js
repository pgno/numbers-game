/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {
			fontFamily: {
				major: ['Major Mono Display', 'monospace']
			}
		}
	},
	plugins: []
};
